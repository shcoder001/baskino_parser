import requests
from bs4 import BeautifulSoup

url = 'https://baskino.org/films/'

response = requests.get(url)
data = BeautifulSoup(response.text, 'html.parser')

for film_data in data.find_all('div', class_='shortpost'):
    link = film_data.div.a['href']
    name = film_data.div.a.img['alt']
    print(name, '------', link)